# ics-ans-role-wazuh-win-clients

Ansible role to install wazuh-win-clients.

## Role Variables

```yaml
wazuh_version: 4.1.5-1
wazuh_server: 1.1.1.1
wazuh_port: 1514
wazuh_winagent_config:
  check_md5: true
  md5: 50455229db051712fef18eb7cda1ce65
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-wazuh-win-clients
```

## License

BSD 2-clause
