import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_ps(host):
    x = host.ansible("raw", "[System.IO.File]::Exists('C:\\Program Files (x86)\\ossec-agent\\ossec-agent.state')", check=False)["stdout_lines"][0]
    assert x == 'True'
